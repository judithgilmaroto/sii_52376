#include <iostream>

#include <sys/types.h>
#include <sys/mman.h>

#include <fcntl.h>

#include <sys/stat.h>

#include <unistd.h>

#include "Esfera.h"

#include "Raqueta.h"

#include "DatosMemCompartida.h"

int main()

{
//Creación del fichero,puntero a la memoria, puntero proyeccion

int file;
DatosMemCompartida* pMemComp;
char* proyeccion;

//Apertura fichero
file=open("/tmp/datosBot.txt",O_RDWR);
//Proyecto el fichero
proyeccion=(char*)mmap(NULL,sizeof(*(pMemComp)),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);

//Cierre
close(file);
//Apunto el puntero de Datos a la proyeccion del fichero en memoria

pMemComp=(DatosMemCompartida*)proyeccion;

//Acciones de control de la raqueta

while(1)

{
usleep(25000);  
float posRaqueta1,posRaqueta2;


posRaqueta1=((pMemComp->raqueta1.y2+pMemComp->raqueta1.y1)/2);
posRaqueta2=((pMemComp->raqueta2.y2+pMemComp->raqueta2.y1)/2);
	
      if(posRaqueta1<pMemComp->esfera.centro.y)

			pMemComp->accion1=1;

		else if(posRaqueta1>pMemComp->esfera.centro.y)

			pMemComp->accion1=-1;
	
        else
                    pMemComp->accion1=0;

		if(posRaqueta2<pMemComp->esfera.centro.y)

			pMemComp->accion2=1;

	       else if(posRaqueta2>pMemComp->esfera.centro.y)

			pMemComp->accion2=-1;
		else

			pMemComp->accion2=0;

}

//Desmontamos la proyeccion de memoria

munmap(proyeccion,sizeof(*(pMemComp)));
}
